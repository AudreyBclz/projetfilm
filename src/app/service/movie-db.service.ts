import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AsyncSubject, map } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class MovieDbService {

  url:string= 'https://api.themoviedb.org/3/discover/movie?include_adult=false&include_video=false&language=en-US&page=1&sort_by=popularity.desc';
  urlpart1Id:string='https://api.themoviedb.org/3/movie/'
  urlpart2Id='?api_key=921724b5280219327c0e6965422b2827';
  urlTest:string='https://api.themoviedb.org/3/movie/550?api_key=921724b5280219327c0e6965422b2827'
  headers:HttpHeaders = new HttpHeaders();
  data:AsyncSubject<any> = new AsyncSubject;

  constructor(private http:HttpClient) {
    this.headers=this.headers.append('Authorization','Bearer eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiI5MjE3MjRiNTI4MDIxOTMyN2MwZTY5NjU0MjJiMjgyNyIsInN1YiI6IjY0ZTQ2M2YzMWZlYWMxMDEzOGQ4NDk1OCIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.FIBT1h8wBvO1KSlvoKZ1h2u0Lp-n4QmEVdCEbiQ11UQ')
    this.headers=this.headers.append('accept','application/json')
  }

   send(){
    return this.http.get(this.url,{headers:this.headers})
   }
   findAfilm(id:number){
    return this.http.get(this.urlpart1Id+id+this.urlpart2Id,{headers:this.headers})
   }
}
