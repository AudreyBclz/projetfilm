import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MovieDbService } from '../service/movie-db.service';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css']
})
export class DetailComponent implements OnInit {

  film:any;
  id:number;
  constructor(private route: ActivatedRoute, private movieService:MovieDbService) {
    this.id=route.snapshot.params['id'];
   }

  ngOnInit(): void {
    console.log(this.id);
    this.film=this.movieService.findAfilm(this.id).subscribe({next: res =>{
      this.film=res;
      console.log(this.film);

    }});
  }

}
