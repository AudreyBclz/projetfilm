import { Component, OnInit } from '@angular/core';
import { MovieDbService } from '../service/movie-db.service';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  films:any;
  constructor(private movieService:MovieDbService) { }

  ngOnInit(): void {
    this.movieService.send().subscribe({
      next:res=>{
        this.films=res;
        console.log(this.films);

     }
    })

  }

}
